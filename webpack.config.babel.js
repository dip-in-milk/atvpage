import path from 'path';
import main from './package';

export default {
  entry: {
    'appletv-page': './src',
  },
  output: {
    path: path.resolve(__dirname, path.dirname(main)),
    filename: path.basename(main),
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
      },
    ],
  },
};
