export default {
  render: data => `<document>
    <loadingTemplate>
      <activityIndicator>
        <title>${data.title || 'Loading...'}</title>
        <description>${data.description}</description>
      </activityIndicator>
    </loadingTemplate>
  </document>`,
};
