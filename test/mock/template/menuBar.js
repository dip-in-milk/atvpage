export default {
  render: data => `<document>
    <menuBarTemplate>
      <menuBar>
        ${data.pages.map(page => `<menuItem page="${page}">
          <title>${page}</title>
        </menuItem>`).join('\n')}
      </menuBar>
    </menuBarTemplate>
  </document>`,
};
