export default {
  render: data => `<document>
    <listTemplate>
      <banner>
        <title>${data.title}</title>
      </banner>
      <list>
        <relatedContent>
          <lockup>
            <img src="http://example.com/images/settings/logo.png" width="542" height="335"/>
          </lockup>
        </relatedContent>
        <section>
          ${data.items.map(item => `<listItemLockup document="${item.document}">
              <title>${item.title}</title>
              <decorationImage src="resource://chevron"/>
            </listItemLockup>`).join('\n')}
        </section>
      </list>
    </listTemplate>
  </document>`,
};
