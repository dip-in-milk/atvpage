import Presenter, { maskChar } from '../../src/Presenter';

global.DOMParser = function DOMParser() {

};

beforeEach(() => {
  global.navigationDocument = {};
});

describe('Presenter', () => {
  describe('#maskChar', () => {
    it('should expose a mask for masking sensitive data', () => expect(typeof maskChar).toBe('string'));
  });

  describe('#getDOMParser', () => {
    const domParser = Presenter.getDOMParser();
    it('should create and return an instance of DOMParser', () => expect(domParser).toBeInstanceOf(global.DOMParser));
    it('should reuse the same instance called for a second time', () => expect(Presenter.getDOMParser()).toEqual(domParser));
  });

  describe('#parse', () => {
    const template = '<template />';
    describe('given contentType', () => {
      it('should call Presenter.domParser.parseFromString with the given string and content type', () => {
        const parser = {
          parseFromString: jest.fn(),
        };
        Presenter.getDOMParser = jest.fn().mockReturnValue(parser);
        Presenter.parse(template, 'custom/xml');
        return expect(parser.parseFromString).toBeCalledWith(template, 'custom/xml');
      });
    });
    describe('using default contentType', () => {
      it('should call Presenter.domParser.parseFromString with the given string', () => {
        const parser = {
          parseFromString: jest.fn(),
        };
        Presenter.getDOMParser = jest.fn().mockReturnValue(parser);
        Presenter.parse(template);
        expect(parser.parseFromString).toBeCalledWith(template, 'application/xml');
      });
    });
  });

  describe('#Default', () => {
    const document = {};
    describe('makeHome = true', () => {
      let makeHome;

      beforeEach(() => {
        makeHome = true;
        global.navigationDocument.clear = jest.fn();
        global.navigationDocument.pushDocument = jest.fn();
        global.navigationDocument.documents = {
          length: 1,
        };
      });

      it('should clear the stack', async () => {
        await Presenter.Default(document, makeHome);
        return expect(global.navigationDocument.clear).toBeCalled();
      });

      it('push the new document into the stack', async () => {
        await Presenter.Default(document, makeHome);
        return expect(global.navigationDocument.pushDocument).toBeCalledWith(document);
      });
    });

    describe('default value', () => {
      beforeEach(() => {
        global.navigationDocument.clear = jest.fn();
        global.navigationDocument.pushDocument = jest.fn();
        global.navigationDocument.documents = {
          length: 1,
        };
      });

      it('should not clear the stack and present the document', async () => {
        await Presenter.Default(document);
        return expect(global.navigationDocument.clear).not.toBeCalled();
      });

      it('should push the new document into the stack', async () => {
        await Presenter.Default(document);
        return expect(global.navigationDocument.pushDocument).toBeCalledWith(document);
      });
    });
  });

  describe('#Remove', () => {
    it('should remove the document from the stack', () => {
      global.navigationDocument.removeDocument = jest.fn();
      const document = {};
      Presenter.Remove(document);
      return expect(global.navigationDocument.removeDocument).toBeCalledWith(document);
    });
  });

  describe('#Replace', () => {
    it('should swap both documnets in the stack', async () => {
      global.navigationDocument.replaceDocument = jest.fn();
      const newPage = {
        document: {},
      };
      const oldPage = {
        document: {},
      };
      await Presenter.Replace(newPage, oldPage);
      return expect(global.navigationDocument.replaceDocument)
        .toBeCalledWith(newPage.document, oldPage.document);
    });
  });

  describe('#ReplaceDocument', () => {
    it('should swap both documnets in the stack', async () => {
      const page = {
        document: {},
      };
      const doc = {};
      global.navigationDocument.replaceDocument = jest.fn();

      await Presenter.ReplaceDocument(page, doc);
      return expect(global.navigationDocument.replaceDocument).toBeCalledWith(page.document, doc);
    });
  });

  describe('#GoHome', () => {
    it('should call navigationDocument.popToRootDocument', () => {
      global.navigationDocument.popToRootDocument = jest.fn();
      Presenter.GoHome();
      return expect(global.navigationDocument.popToRootDocument).toBeCalled();
    });
  });

  describe('#Modal', () => {
    it('should call navigationDocument.presentModal', () => {
      global.navigationDocument.presentModal = jest.fn();
      const documemnt = {};
      Presenter.Modal(documemnt);
      return expect(global.navigationDocument.presentModal).toBeCalledWith(documemnt);
    });
  });
  describe('#DismissModal', () => {
    it('should call navigationDocument.dismissModal', () => {
      global.navigationDocument.dismissModal = jest.fn();
      Presenter.DismissModal();
      return expect(global.navigationDocument.dismissModal).toBeCalled();
    });
  });

  describe('#MenuBarPage', () => {
    const feature = {};
    const page = {};
    const event = {
      target: {
        parentNode: {},
      },
    };

    beforeEach(() => {
      page.present = jest.fn();
      feature.getDocument = jest.fn();
      feature.setDocument = jest.fn();
      event.target.parentNode.getFeature = jest.fn().mockReturnValue(feature);
    });

    it('should get MenuBarDocument Feature from parentNode', () => {
      Presenter.MenuBarPage(event, page);
      return expect(event.target.parentNode.getFeature).toBeCalledWith('MenuBarDocument');
    });

    it('should get feature`s document', () => {
      Presenter.MenuBarPage(event, page);
      return expect(feature.getDocument).toBeCalledWith(event.target);
    });

    it('should not present the new document if feature.getDocument() returns truthy value', () => {
      feature.getDocument.mockReturnValue(true);

      Presenter.MenuBarPage(event, page);
      return expect(feature.getDocument).toBeCalledWith(event.target);
    });

    it('should present the new document if feature.getDocument() returns falsey value', () => {
      feature.getDocument.mockReturnValue(false);

      Presenter.MenuBarPage(event, page);
      return expect(feature.getDocument).toBeCalledWith(event.target);
    });

    it('shouldn`t call feature.getDocument if feature doesn`t exists', () => {
      event.target.parentNode.getFeature.mockReturnValue(false);

      Presenter.MenuBarPage(event, page);
      expect(feature.getDocument).not.toBeCalled();
      expect(page.present).not.toBeCalled();
    });

    it('should call page.preset()', () => {
      event.target.parentNode.getFeature.mockReturnValue(feature);

      Presenter.MenuBarPage(event, page);
      expect(feature.getDocument).toBeCalledWith(event.target);
      expect(page.present).toBeCalledWith(expect.any(Function));
    });

    it('should call page.present to present the document', () => {
      Presenter.MenuBarPage(event, page);

      const document = {};
      page.present.mock.calls[0][0](document);
      return expect(feature.setDocument).toBeCalledWith(document, event.target);
    });
  });

  describe('#Asset', () => {
    it('should present the given player', () => {
      const player = {
        present: jest.fn(),
      };
      Presenter.Asset(player);
      return expect(player.present).toBeCalled();
    });
  });

  describe('#isPresented', () => {
    it('should check if the documemnt is present in the stack', () => {
      global.navigationDocument.documents = {
        includes: jest.fn(),
      };
      const document = 'asd';
      Presenter.isPresented(document);
      return expect(global.navigationDocument.documents.includes).toBeCalledWith(document);
    });
  });

  describe('#mask', () => {
    const input = 'some string';
    const masked = Presenter.mask(input);
    it('should have output length of input length times mask length', () => expect(masked).toHaveLength(input.length * maskChar.length));
    it('should have output containing only mask symbols', () => expect([...masked]).toContain(maskChar));
  });
});
