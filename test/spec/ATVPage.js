import Presenter from '../../src/Presenter';
import ATVPage from '../../src';

global.navigationDocument = {};
jest.mock('../../src/Presenter', () => ({
  makeDocument: () => jest.fn(),
}));

describe('ATVPage', () => {
  describe('constructor options', () => {
    it('all options should be optional', () => expect(new ATVPage()).toBeInstanceOf(ATVPage));
  });

  describe('#getDocument', () => {
    const template = () => '<template />';
    Presenter.parse = jest.fn();

    it('should call the Presenter.parse', async () => new ATVPage({
      template,
    }).getDocument().then(() => expect(Presenter.parse).toBeCalledWith('<template />')));
  });

  describe('options.data', () => {
    it('should work with Objects', async () => {
      const page = new ATVPage({
        data: {
          gKey: 'dData',
          key1: 'value1',
        },
      });
      const data = await page.getData();
      return expect(data).toEqual({
        gKey: 'dData',
        key1: 'value1',
      });
    });

    describe('once data is changed', () => {
      const options = {
        data: {
          key1: 'value1',
        },
      };

      global.navigationDocument.pushDocument = jest.fn();
      // const presented = page.present();
      it('should delete cached document', () => {
        const page = new ATVPage(options);
        return page.present().then(() => {
          page.setData({});
          return expect(page).not.toHaveProperty('document');
        });
      });
      it('should delete cached template', async () => {
        const page = new ATVPage(options);
        await page.present();
        page.setData({});
        return expect(page).not.toHaveProperty('template');
      });
    });

    it('should work with Promises', async () => {
      ATVPage.setGlobal('gKey', 'gData');
      const data = await new ATVPage({
        data: Promise.resolve({
          val: 88,
        }),
      }).getData();
      return expect(data).toEqual({
        gKey: 'gData',
        val: 88,
      });
    });
  });

  describe('options.presenter', () => {
    it('should accept String as Presenter method name', () => {
      Presenter.SomePresenterMethodName = jest.fn();
      const page = new ATVPage({
        presenter: 'SomePresenterMethodName',
      });

      return Promise.all([
        page.getDocument(),
        page.present(),
      ]).then(([
        document,
      ]) => expect(Presenter.SomePresenterMethodName).toBeCalledWith(document));
    });

    it('should accept Function', () => {
      const presenter = jest.fn();
      const page = new ATVPage({
        presenter,
      });
      return Promise.all([
        page.getDocument(),
        page.present(),
      ]).then(([
        document,
      ]) => expect(presenter).toBeCalledWith(document));
    });
  });

  describe('.present() method', () => {
    it('should be presenter with secondPresenter only', async () => {
      const firstPresenter = jest.fn();
      const secondPresenter = jest.fn();
      await new ATVPage({
        presenter: firstPresenter,
      }).present(secondPresenter);
      return expect(firstPresenter).not.toBeCalled()
      && expect(secondPresenter).toBeCalled();
    });
  });

  describe('#setData()', () => {
    let page;

    beforeEach(() => {
      page = new ATVPage({
        data: {
          oldKey: 'oldValue',
        },
      });
    });

    describe('given a function', () => {
      const data = jest.fn(oldData => ({
        ...oldData,
        newKey: 'new value',
      }));

      it('should delete cached template and document and set data', async () => {
        const oldData = await page.data;
        page.setData(data);

        expect(page).not.toHaveProperty('template');
        expect(page).not.toHaveProperty('document');

        await page.data;

        expect(data).toHaveBeenCalledTimes(1);
        expect(data).toHaveBeenCalledWith(oldData);
      });
    });

    describe('given an object', () => {
      const data = {};

      it('should delete cached template and document and set data', () => {
        page.setData(data);

        expect(page).not.toHaveProperty('template');
        expect(page).not.toHaveProperty('document');

        expect(page.data).resolves.toEqual({});
      });
    });
  });

  describe('#getTemplate', () => {
    let page;

    beforeEach(() => {
      page = new ATVPage({

      });
      page.render = jest.fn().mockResolvedValue('');
    });

    describe('page has cached template', () => {
      it('should return the cached template and not call render', () => {
        page.template = Promise.resolve('');
        expect(page.getTemplate()).resolves.toEqual('');
        expect(page.render).not.toBeCalled();
      });
    });

    describe('page doesn`t have cached template', () => {
      it('should render and cache the template', () => {
        delete page.template;

        expect(page.getTemplate()).resolves.toEqual('');
        expect(page.template).resolves.toEqual('');
        expect(page.render).toBeCalled();
      });
    });
  });
});

describe('ATVPage.setGlobal( key, value )', () => {
  ATVPage.setGlobal('gKey', 'gData');

  it('should sets a global variable', () => expect(ATVPage.getGlobal('gKey')).toEqual('gData'));

  it('should be accesible by all pages', async () => {
    const data = await (new ATVPage({
      data: { key2: 'value2' },
    })).getData();
    return expect(data).toEqual({
      gKey: 'gData',
      key2: 'value2',
    });
  });

  it('should not overrwrite variable from Page data', async () => {
    const page = new ATVPage({
      data: { gKey: 'gData2' },
    });
    const data = await page.getData();
    return expect(data).toEqual({
      gKey: 'gData2',
    });
  });
});
