const SAFE_INTV = 1000;

export const maskChar = '•';
const CONTENT_TYPE = {
  TEMPLATE: 'application/xml',
};

let domParser;

/**
 * Presenter class used to present Page in the navigationDocument
 */
export default class Presenter {
  static getDOMParser() {
    if (!domParser) {
      domParser = new global.DOMParser();
    }
    return domParser;
  }

  /**
   * Parses the given string using Presenter.parser
   */
  static parse(string, contentType = CONTENT_TYPE.TEMPLATE) {
    return Presenter.getDOMParser().parseFromString(string, contentType);
  }

  /**
   * Page presenters should always call Presenter.Default
   * which will push the given document to the stack unless it has
   * already been pushed
   * @param {DOMDocument} document DOMDocument
   * @param {boolean} makeHome if set to true all the existing documents in
   * the stack will be removed
   * @returns {Promise}
   */
  static Default(document, makeHome = false) {
    return new Promise(((resolve) => {
      if (makeHome && global.navigationDocument.documents.length) {
        global.navigationDocument.clear();
        setTimeout(() => resolve(), SAFE_INTV);
      } else {
        resolve();
      }
    })).then(() => global.navigationDocument.pushDocument(document));
  }

  /**
   * Removes the given document from the stack
   * @param {DOMDocument} document DOMDocument
   */
  static Remove(document) {
    return global.navigationDocument.removeDocument(document);
  }

  static Replace(newPage, oldPage) {
    return Promise.all([newPage.document, oldPage.document])
      .then(docs => new Promise((resolve) => {
        global.navigationDocument.replaceDocument(...docs);
        setTimeout(() => resolve(), SAFE_INTV);
      }));
  }

  /**
   * Replaces the given DOMDocument with the given page
   * @param {Page} page page instance
   * @param {DOMDocument} doc DOMDocument to be replaced
   * @returns {Promise<Array>}
   */
  static ReplaceDocument(page, doc) {
    return Promise.all([
      page.document,
      doc,
    ]).then(docs => global.navigationDocument.replaceDocument(...docs));
  }

  /**
   * Removes all the documents from the stack but the first one
   */
  static GoHome() {
    global.navigationDocument.popToRootDocument();
  }

  /**
   * Presents the given DOMDocument as Modal
   * @param {DOMDocument} document DOMDocument
   */
  static Modal(document) {
    return global.navigationDocument.presentModal(document);
  }

  /**
   * Dismisses a presented modal
   */
  static DismissModal() {
    return global.navigationDocument.dismissModal();
  }

  /**
   * Presents the page as a Menu Bar Page
   * @param {Event} event Event
   * @param {Page} page Page
   */
  static MenuBarPage(event, page) {
    const element = event.target;
    const feature = element.parentNode.getFeature('MenuBarDocument');

    if (feature) {
      const currentDocument = feature.getDocument(element);

      if (!currentDocument) {
        page.present(document => feature.setDocument(document, element));
      }
    }
  }

  /**
   * Default Asset Page presenter
   * @param {Player} player Player
   */
  static Asset(player) {
    return player.present();
  }

  /**
   * Checks if the given document is in the stack
   * @param {DOMDocument} document DOMDocument
   * @returns {boolean}
   */
  static isPresented(document) {
    return global.navigationDocument.documents.includes(document);
  }

  /**
   * Masks the given string with the mask character
   * @param {string} string string to be masked
   * @returns {string}
   */
  static mask(string) {
    return string.replace(/./g, maskChar);
  }
}
