import Presenter from './Presenter';

const defaults = {
  template: data => `${data}`,
  presenter: Presenter.Default,
  data: {},
};

const gdata = {};

export default class ATVPage {
  constructor(options) {
    this.settings = {
      ...defaults,
      ...options,
    };
    this.data = Promise.resolve(this.settings.data);
  }

  setData(data) {
    delete this.template;
    delete this.document;
    if (typeof (data) === 'function') {
      this.data = this.data.then(data);
    } else {
      this.data = Promise.resolve(data);
    }
  }

  getDocument() {
    if (!this.document) {
      this.document = this.getTemplate().then(template => Presenter.parse(template));
      // this.document = Presenter.parse(await this.getTemplate());
    }
    return this.document;
  }

  render() {
    const { template } = this.settings;
    return this.getData().then(data => template(data));
    // return (template.render || template)(await this.getData());
  }

  getTemplate() {
    if (!this.template) {
      this.template = this.render();
    }
    return this.template;
  }

  getData() {
    return Promise.all([
      this.constructor.getGlobal(),
      this.data,
    ]).then(([
      globalData,
      data,
    ]) => ({
      ...globalData,
      ...data,
    }));
  }

  static setGlobal(key, value) {
    gdata[key] = value;
  }

  static getGlobal(key) {
    if (key) {
      return gdata[key];
    }
    return gdata;
  }

  present(presenter = this.settings.presenter) {
    // return presenter(await this.getDocument());
    return this.getDocument().then(presenter in Presenter ? Presenter[presenter] : presenter);
  }
}
