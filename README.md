# AppleTV Page library

## Installation
```bash
npm install @dip-in-milk/atvpage
```

## Usage
### Page.prototype.constructor
```js
import ATVPage from '@dip-in-milk/atvpage';
var page = new ATVPage( options );
```

#### options
```js
var options = {
	presenter: 'Modal',
	data: async () => await Source.getData(),
	template: data => `<alertTemplate>
		<title>${data.title}</title>
		${data.buttons.map(button => `<button>
			<text>${button.text}</text>
		</button>`)}
	</alertTemplate>`
};
```

### ATVPage.prototype.present([presenter])
The present method triggers fetching the data and loading of the template, once finished renders the template with the given data and presents it with the given presenter, or uses the construct-time presenter

## Extending ATVPage
```js
class UserPage extends ATVPage {

    constructor(...opts) {
        super(...opts);
        this.template = 'user/main';
    }

    this.selectHandler(event) {
        const { target } = event;
        // manipulate target
    }
   
    async document() {
        const doc = await super.document(...arguments);
        doc.addEventListener('select', this.selectHandler);
        return doc;
    }
}
```